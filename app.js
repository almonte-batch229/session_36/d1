// Setup the modules/dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes.js");

// Setup the server
const app = express();
const port = 4001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Adding taskRoutes
// allows all the task routes created in taskRoutes.js file to use "/" task routes
app.use("/tasks", taskRoutes)

// Database Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.3mzqerb.mongodb.net/B229_to-do?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the coud database"));

// Server Listening
app.listen(port, () => console.log(`Server is running at port ${port}.`));

// Setup the modules/dependencies
const express = require("express");
const router = express.Router();
const taskControllers = require("../controllers/taskControllers.js")

// [SECTION] Routes
// The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
// They invoke the controller functions from the controller files
// All the business logic is done in the controller

// Get All Tasks
router.get("/", (req, res) => {
	taskControllers.getAllTasks().then(resultFromController => {res.send(resultFromController)});
});

// Create a Task
router.post("/", (req, res) => {
	taskControllers.createTasks(req.body).then(resultFromController => {res.send(resultFromController)});
});

// Delete a Task
router.delete("/:id", (req, res) => {
	taskControllers.deleteTask(req.params.id).then(resultFromController => {res.send(resultFromController)});
});

// Update a Task
router.put("/:id", (req, res) => {
	taskControllers.updateTask(req.params.id, req.body).then(resultFromController => {res.send(resultFromController)});
});

// Get a Specific Task
router.get("/:id", (req, res) => {
	taskControllers.getTask(req.params.id).then(resultFromController => {res.send(resultFromController)});
});

// Update status to "complete"
router.put("/:id/complete", (req, res) => {
	taskControllers.completeTask(req.params.id).then(resultFromController => {res.send(resultFromController)});
});

module.exports = router;